Backend (server side logic & Data Resource) code repository for Pondicherry UniversityBus Tracker Application

Requirements:
  - GNU/Linux Operating System
  - Git DVCS
  - Python 2.7.x
  - Flask Micro Webframework
  - DB (SQL / NoSQL Not yet decided)


List of Scenarios & Data flow [here](https://pad.riseup.net/p/pu-bus-tracker)
